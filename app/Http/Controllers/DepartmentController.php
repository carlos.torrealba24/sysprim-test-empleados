<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
        // función para llamar la tabla con todos los registros
        public function index(){
                $departments = Department::where('status','=','A')->paginate(10);
                return view('Department.index', compact('departments'));
        }

        // función para llamar el formulario
        public function create(){
                return view('Department.create');
        }

        // función para guardar un registro nuevo
        public function store(DepartmentRequest $request){
                Department::create($request->validated());
                return response()->json(['success' => true]);
        }

        // función para llamar el formulario con información del registro a editar
        public function edit($id){
                $departments = Department::where('id', '=', $id)->get()->first();
                return view('Department.edit', compact('departments'));
        }

        // función para modificar un registro
        public function update(DepartmentRequest $request, $id){
                $departments = Department::where('id','=',$id)->get()->first();
                $departments->update($request->validated());
                return response()->json(['success' => true]);
        }

        // función para eliminar logicamente un registro con softdelete
        public function destroy($id){
                $departments = Department::where('id','=',$id)->get()->first();
                $departments->delete();
                return response()->json(['success' => true]);
        }
}
