<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeByDepartmentRequest;
use App\Models\Employee;
use App\Models\Department;
use App\Models\EmployeeByDepartment;
use Illuminate\Http\Request;

class EmployeeByDepartmentController extends Controller
{
        // función para llamar la tabla con todos los registros
        public function index(){
                $employeesByDepartments = Employee::where('status','=','A')->paginate(10);
                return view('EmployeeByDepartment.index', compact('employeesByDepartments'));
        }

        // función para llamar el formulario
        public function newCreate($id){
                $departments = Department::where('status','=','A')->with('employees')->get();
                $employees = Employee::where('id','=',$id)->with('departments')->get()->first();
                return view('EmployeeByDepartment.create', compact('employees', 'departments'));
        }

        // función para guardar un registro nuevo
        public function store(EmployeeByDepartmentRequest $request){
                $employees = Employee::where('id','=',$request->id_employees)->get()->first();
                $validated = $request->validated();
                if($validated){
                        $employees->departments()->sync($request->id_departments);
                }
        }

        // función para llamar el formulario con información del registro a editar
        public function edit($id){
                $employeeByDepartment = Employee::where('id','=',$id)->with('departments')->get()->first();
                $dep = Department::where('status','=','A')->get();
                return view('EmployeeByDepartment.edit', compact('employeeByDepartment', 'dep'));
                
        }

        // función para modificar un registro
        public function update(Request $request){
                $employees = Employee::where('id','=',$request->id_employees)->get()->first();
                if($request->id_departments){
                        foreach($request->id_departments as $registros){
                                $employees->departments()->detach($registros);
                        }
                }
                return response()->json(['success' => true]);
                
        }

        // función para eliminar logicamente un registro
        public function destroy($id){
                $employees = Employee::where('id','=',$id)->get()->first();
                $employees->delete();
                return response()->json(['success' => true]);
        }
}
