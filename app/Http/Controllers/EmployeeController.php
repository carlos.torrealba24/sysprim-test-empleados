<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Gender;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    // función para llamar la tabla con todos los registros
    public function index(){
        $employees = Employee::where('status','=','A')->with('gender')->paginate(10);
        return view('Employee.index', compact('employees'));
    }

    // función para llamar el formulario
    public function create(){
        $generos = Gender::where('status','=','A')->get();
        return view('Employee.create', compact('generos'));
    }

    // función para guardar un registro nuevo
    public function store(EmployeeRequest $request){
        Employee::create($request->validated());
        return response()->json(['success' => true]);
    }

    // función para llamar el formulario con información del registro a editar
    public function edit($id){
        $employees = Employee::where('id','=',$id)->get()->first();
        $generos = Gender::where('status','=','A')->get();
        return view('Employee.edit', compact('generos', 'employees'));
    }

    // función para modificar un registro
    public function update(EmployeeRequest $request, $id){
        $employees = Employee::where('id','=',$id)->get()->first();
        $employees->update($request->validated());
        return response()->json(['success' => true]);
    }

    // función para eliminar permanentemente un registro
    public function destroy($id){
        $employees = Employee::where('id','=',$id)->get()->first();
        $employees->delete();
        return response()->json(['success' => true]);
    }
}
