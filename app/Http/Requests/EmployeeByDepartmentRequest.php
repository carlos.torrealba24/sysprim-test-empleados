<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeByDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        if ($this->isMethod('post')) {
            return [
                'id_employees' => 'required',
                'id_departments' => 'required',
            ];
        } else {
            return [
                'id_employees' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'id_employees.required' => 'El empleado es obligatorio.',
            'id_departments.required' => 'El departamento es obligatorio.',
        ];
    }
}
