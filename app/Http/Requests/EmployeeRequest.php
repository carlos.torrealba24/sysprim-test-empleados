<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        if ($this->isMethod('post')) {
            return [
                'name' => 'required|min:3|max:30',
                'last_name' => 'required|min:3|max:30',
                'cedula' => 'required|unique:employees|min:6|max:10',
                'id_gender' => 'required',
                'birthday' => 'required|date|date_format:Y-m-d',
                'years_old' => 'required'
            ];
        } else {
            return [
                'name' => 'required|min:3|max:30',
                'last_name' => 'required|min:3|max:30',
                'cedula' => 'required|unique:employees,cedula,'.$this->id.'|min:6|max:10',
                'id_gender' => 'required',
                'birthday' => 'required|date|date_format:Y-m-d',
                'years_old' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'cedula.required' => 'La cedula es obligatoria.',
            'cedula.unique' => 'La cedula ya existe en el sistema.',
            'cedula.min' => 'La cedula debe contener al menos 6 caracteres.',
            'cedula.max' => 'La cedula debe contener máximo 10 caracteres.',

            'name.required' => 'El nombre es obligatorio.',
            'name.min' => 'El nombre debe contener al menos 3 caracteres.',
            'name.max' => 'El nombre debe contener máximo 30 caracteres.',

            'last_name.required' => 'El apellido es obligatorio.',
            'last_name.min' => 'El apellido debe contener al menos 3 caracteres.',
            'last_name.max' => 'El apellido debe contener máximo 30 caracteres.',
            
            'id_gender.required' => 'El género es obligatorio.',

            'birthday.required' => 'La fecha es obligatoria.',
        ];
    }
}
