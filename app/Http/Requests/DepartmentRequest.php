<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        if ($this->isMethod('post')) {
            return [
                'name' => 'required|unique:departments|min:5|max:30',
                'description' => 'required|min:3|max:30',
            ];
        } else {
            return [
                'name' => 'required|unique:departments,name,'.$this->id.'|min:5|max:30',
                'description' => 'required|min:3|max:30',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio.',
            'name.unique' => 'El nombre ya existe en el sistema.',
            'name.min' => 'El nombre debe contener al menos 5 caracter.',
            'name.max' => 'El nombre debe contener máximo 30 caracteres.',

            'description.required' => 'La descripción es obligatoria.',
            'description.min' => 'La descripción debe contener al menos 5 caracter.',
            'description.max' => 'La descripción debe contener máximo 30 caracteres.',
        ];
    }
}
