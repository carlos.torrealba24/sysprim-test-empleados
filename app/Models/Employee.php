<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'last_name',
        'cedula',
        'birthday',
        'id_gender',
        'years_old',
        'status',
        'email',
        'id',
    ];

    public function gender(){
        return $this->belongsTo(Gender::class, 'id_gender');
    }
    public function departments(){
        return $this->belongsToMany(Department::class);
    }

}
