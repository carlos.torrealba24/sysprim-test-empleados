<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'cedula' => $this->faker->numberBetween(4000000,40000000),
            'id_gender' => $this->faker->randomElement(['1', '2', '3']),
            'birthday' => $this->faker->dateTime(),
            'years_old' => $this->faker->numberBetween(18,70),
        ];
    }
}
