<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::factory()->count(20)->create()->each(function ($u) {
            if($u->years_old < 60){
                $u->departments()->sync(random_int(1, 5));
            }
        });
    }
}
