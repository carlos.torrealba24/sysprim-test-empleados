<?php

namespace Database\Seeders;

use App\Models\Gender;
use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::Create([
            'name' => 'F',
            'description' => 'Femenino',
        ]);
        Gender::Create([
            'name' => 'M',
            'description' => 'Masculino',
        ]);
        Gender::Create([
            'name' => 'O',
            'description' => 'Otros',
        ]);
    }
}
