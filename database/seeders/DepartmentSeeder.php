<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::Create([
            'name' => 'Administración',
            'description' => 'Departamento de finanzas',
        ]);
        Department::Create([
            'name' => 'Compras',
            'description' => 'Departamento encargado del inventario',
        ]);
        Department::Create([
            'name' => 'Informática',
            'description' => 'Departamento de mantenimiento de software y hardware',
        ]);
        Department::Create([
            'name' => 'Soporte',
            'description' => 'Departamento de atención al cliente',
        ]);
        Department::Create([
            'name' => 'Ventas',
            'description' => 'Departamento encargado de la facturación',
        ]);

    }
}
