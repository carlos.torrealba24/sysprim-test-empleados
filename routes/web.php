<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeByDepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Models\EmployeeByDepartment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
    Route::resource('department', DepartmentController::class);
    Route::resource('employee', EmployeeController::class);
    Route::resource('employees-by-departments', EmployeeByDepartmentController::class);
    //Route::delete('/employee/disincorporate/{id}', [EmployeeController::class, 'disincorporate'])->name('employee.disincorporate');
    Route::get('employees-by-departments/newcreate/{id}', [EmployeeByDepartmentController::class, 'newCreate'])->name('employees-by-departments.newcreate');
});
