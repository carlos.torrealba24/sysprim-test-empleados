@extends('layouts.app')

@section('content')
    <div class="">
        <form action="" id="formEmpleado" method="post">
            @csrf  
            @method('PUT')
            <div>
                <ul id="tasks" class="mt-3 list-disc list-inside text-sm text-red-600">
     
                </ul>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Empleado:</label>
                        <input class="form-control" type="text" name="id_employee" id="id_employee" required disabled valor="{{$employeeByDepartment->id}}" value="{{$employeeByDepartment->name.' '. $employeeByDepartment->last_name }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Departamentos a desincorporar:</label>
                        <select class="form-control selectpicker" name="id_departments" id="id_departments" placeholder="Seleccione uno o más departamentos" multiple required>
                            @foreach($employeeByDepartment->departments as $dptos)
                                <option value="{{$dptos->id}}">{{$dptos->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="float-right">
                <button class="btn btn-success" id="btnEditEmpleadoPorDepartamento">Desincorporar</button>
                <a class="btn btn-warning" href="" id="btnBack">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@push('javascript')

    <script>
        $('#btnEditEmpleadoPorDepartamento').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let id_employees    = $('#id_employee').attr('valor');
            let id_departments  = $('#id_departments').val();
            let id              = $('#id').val();
            let _token          = $('input[name=_token]').val();
            //Petición ajax
            $.ajax({
                type: "PUT",
                url: "{{route('employees-by-departments.update', $employeeByDepartment->id)}}",
                data: {
                    id              : id,
                    id_employees    : id_employees,
                    id_departments  : id_departments,
                    _token          : _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Modificación exitosa', 'Modificar registro');
                        window.location.href = '/employees-by-departments'
                    }, 1000);
                },
                error: function (err) {
                    if(err.status == 422){
                        let template = `
                            <div class="card alert-icon flex-1 items-center border-2 border-red-500 justify-center rounded-full mt-2">

                            `;

                        $.each(err.responseJSON.errors, function (i,error) {
                        template += `
                                <li class="ml-2 text-danger">
                                    ${error[0]}
                                </li>
                                `
                        });
                        template += `</div>`;
                        $('#tasks').html(template);
                    }
                }   
            });
        });
    </script>

    <script>
    $('#btnBack').on('click', function(e){
        $.ajax({
                type: "GET",
                url: "{{route('employees-by-departments.index')}}",
                success:function(response){
                    window.location.href = '/employees-by-departments'
                },
        });
    });
    </script>
@endpush


