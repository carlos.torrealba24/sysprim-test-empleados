@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mb-5 mt-5">
            <div class="col-md-6" style="display:flex;">
                <h3 class="text-secondary">Asignar Dptos</h3>
                {{-- <a href="" class="btn btn-primary" style="margin-left: 30px;" id="btnAsignarEmpleadosDepartamentos">Asignar Dptos</a> --}}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="table-responsive col-md-12">
        <table class="table table-bordered table-hover" id="table-generic">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Departamento</th>
                    <th class="text-center">Edad</th>
                    <th class="text-center">Asignar</th>
                    <th class="text-center">Desincorporar</th>
                    <th class="text-center">Despedir</th>
                </tr>
            </thead>
            <tbody>
                @if (count($employeesByDepartments) > 0)
                    @foreach ($employeesByDepartments as $ebd)
                        <tr id="rowId-{{$ebd->id}}">
                            <td class="text-center">
                                {{$ebd->id}}
                            </td>
                            <td class="text-center">
                                {{$ebd->name . ' ' . $ebd->last_name}}
                            </td>
                            <td class="text-center">
                                @foreach($ebd->departments as $dpto)
                                    {{$dpto->name}}
                                @endforeach
                            </td>
                            <td class="text-center">
                                {{$ebd->years_old}}
                            </td>
                            <td class="text-center">
                                @if ($ebd->years_old <= 60)    
                                <a href="{{route('employees-by-departments.newcreate', $ebd->id)}}" valor="{{$ebd->id}}" id="newCreate" class="btn btn-outline-primary" title="Asignar"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                    </svg>
                                </a>
                                @endif
                            </td>
                            <td class="text-center">
                            @if ($ebd->years_old <= 60 && count($ebd->departments)>0)
                                        <a href="{{route('employees-by-departments.edit', $ebd->id)}}" id="btnDesincorporar" class="btn btn-outline-warning" title="Desincorporar">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-x" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.146 7.146a.5.5 0 0 1 .708 0L8 8.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 9l1.147 1.146a.5.5 0 0 1-.708.708L8 9.707l-1.146 1.147a.5.5 0 0 1-.708-.708L7.293 9 6.146 7.854a.5.5 0 0 1 0-.708z"/>
                                            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
                                            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
                                            </svg>
                                        </a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if (count($ebd->departments)==0) 
                                    <form action="" style="display: inline" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button id="btnDelete" valor2="{{$ebd->id}}" class="btn btn-outline-danger" title="Despedir">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                        <tr>
                            <td colspan="5" class="text-center">No hay registros en el sistema</td>
                        </tr>
                @endif
                
            </tbody>
        </table>
        <div class="d-flex justify-content-end">
            {{ $employeesByDepartments->links() }}
        </div>
    </div>

@endsection

@push('javascript')
    <script>
        // Ajax para eliminar un registro
        $(document).on('click', '#btnDelete', function(e){
            if(confirm('Está seguro que quiere despedir al empleado?')){
                e.preventDefault();
                let id = $(this).attr('valor2');
                let _token = $('input[name=_token]').val();
                $.ajax({
                    type: "DELETE",
                    url: `employees-by-departments/${id}`,
                    data: {
                        id: id,
                        _token: _token
                    },
                    success:function(response){
                        toastr.success('Registro Eliminado', 'Eliminar', 3000);
                        $('#rowId-'+id).remove();
                    },
                    error: function (err) {
                        if(err.status == 422){
                            $.each(err.responseJSON.errors, function (i,error) {
                                toastr.success('error en eliminacion', +error[0], 3000);
                            })
                        }
                    }
                });
            }else{
                e.preventDefault();
            }
        });
    </script>
@endpush
