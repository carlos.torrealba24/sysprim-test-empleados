@extends('layouts.app')

@section('content')
    <form action="" id="formEmpleadoPorDepartamento" method="post">
        @csrf  
        <div>
            <ul id="tasks" class="mt-3 list-disc list-inside text-sm text-red-600">
 
            </ul>
        </div>
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Empleado:</label>
                    <input class="form-control" type="text" name="id_employee" id="id_employee" required disabled valor="{{$employees->id}}" value="{{$employees->name.' '. $employees->last_name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Departamentos:</label>
                    <select class="form-control selectpicker" name="id_departments" id="id_departments" placeholder="Seleccione uno o más departamentos" multiple required>
                        @foreach ($departments as $department)
                            <option value="{{$department->id}}">{{$department->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="float-right">
            <button class="btn btn-success" id="btnAsignarEmpleado">Asignar</button>
            <a class="btn btn-warning" href="" id="btnBack">Atras</a>
        </div>
    </form>
    
@endsection

@push('javascript')
    <!-- Petición ajax para guardar un registro en la base de datos -->
    <script>
        $('#btnAsignarEmpleado').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let id_employees    = $('#id_employee').attr('valor');
            let id_departments  = $('#id_departments').val();
            let _token          = $('input[name=_token]').val();
            //Petición ajax
            $.ajax({
                type: "POST",
                url: "{{route('employees-by-departments.store')}}",
                data: {
                    id_employees    : id_employees,
                    id_departments  : id_departments,
                    _token          : _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Registro exitoso', 'Nuevo registro');
                        window.location.href = '/employees-by-departments/';
                    }, 1000);
                },
                error: function (err) {
                    if(err.status == 422){
                        let template = `
                            <div class="card alert-icon flex-1 items-center border-2 border-red-500 justify-center rounded-full mt-2">

                            `;

                        $.each(err.responseJSON.errors, function (i,error) {
                        template += `
                                <li class="ml-2 text-danger">
                                    ${error[0]}
                                </li>
                                `
                        });
                        template += `</div>`;
                        $('#tasks').html(template);
                    }
                }   
            });
        });
    </script>
    <script>
        $('#btnBack').on('click', function(e){
            $.ajax({
                    type: "GET",
                    url: "{{route('employees-by-departments.index')}}",
                    success:function(response){
                        window.location.href = '/employees-by-departments'
                    },
            });
        });
    </script>
@endpush