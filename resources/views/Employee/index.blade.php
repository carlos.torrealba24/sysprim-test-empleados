@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mb-5 mt-5">
            <div class="col-md-6" style="display:flex;">
                <h3 class="text-secondary">Empleados</h3>
                <a href="" class="btn btn-primary" style="margin-left: 30px;" id="btnNuevoEmpleado">Nuevo empleado</a>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="table-responsive col-md-12">
        <table class="table table-bordered table-hover" id="table-generic">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Cedula</th>
                    <th class="text-center">Fecha Nac.</th>
                    <th class="text-center">Edad</th>
                    <th class="text-center">Género</th>
                    <th class="text-center">Editar</th>
                    <th class="text-center">Eliminar</th>

                </tr>
            </thead>
            <tbody>
                @if (count($employees) > 0)
                    @foreach ($employees as $employee)
                        <tr id="rowId-{{$employee->id}}">
                            <td class="text-center">
                                {{$employee->id}}
                            </td>
                            <td class="text-center">
                                {{$employee->name . ' ' . $employee->last_name}}
                            </td>
                            <td class="text-center">
                                {{$employee->cedula}}
                            </td>
                            <td class="text-center">
                                {{$employee->birthday}}
                            </td>
                            <td class="text-center">
                                {{$employee->years_old}}
                            </td>
                            <td class="text-center">
                                {{$employee->gender->description}}
                            </td>
                            <td class="text-center">
                                <a href="{{route('employee.edit', $employee->id)}}" id="editBtn" class="btn btn-outline-warning" title="Modificar"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                    </svg>
                                </a>
                            </td>
                            <td class="text-center">
                                <form action="" style="display: inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button id="btnDelete" valor="{{$employee->id}}" class="btn btn-outline-danger" title="Eliminar">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                        <tr>
                            <td colspan="5" class="text-center">No hay registros en el sistema</td>
                        </tr>
                @endif
                
            </tbody>
        </table>
        <div class="d-flex justify-content-end">
            {{ $employees->links() }}
        </div>
    </div>

@endsection

@push('javascript')
    <!-- Petición ajax para llamar a la vista create -->
    <script>
        $('#btnNuevoEmpleado').on('click', function(e){
            // Petición ajax
            $.ajax({
                type: "GET",
                url: "{{route('employee.create')}}",
                success: function() { 
                    window.location.href = '/employee/create'
                },
                error: function(xhr, ajaxOptions, thrownerror) { }
            })
        });
    </script>
    <script>
        // Ajax para eliminar un registro
        $(document).on('click', '#btnDelete', function(e){
            if(confirm('Está seguro que quiere despedir al empleado?')){
                e.preventDefault();
                let id = $(this).attr('valor');
                let _token = $('input[name=_token]').val();
                $.ajax({
                    type: "DELETE",
                    url: `employee/${id}`,
                    data: {
                        id: id,
                        _token: _token
                    },
                    success:function(response){
                        toastr.success('Registro Eliminado', 'Eliminar', 3000);
                        $('#rowId-'+id).remove();
                    },
                    error: function (err) {
                        if(err.status == 422){
                            $.each(err.responseJSON.errors, function (i,error) {
                                toastr.success('error en eliminacion', +error[0], 3000);
                            })
                        }
                    }
                });
            }else{
                e.preventDefault();
            }
        });
    </script>
@endpush
