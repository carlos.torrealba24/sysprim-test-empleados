@extends('layouts.app')

@section('content')
    <div class="">
        <form action="" id="formEmpleado" method="post">
            @csrf  
            <div>
                <ul id="tasks" class="mt-3 list-disc list-inside text-sm text-red-600">
     
                </ul>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="escribe un nombre" aria-describedby="helpId" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Apellido:</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="escribe un apellido" aria-describedby="helpId" required>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Cedula:</label>
                        <input type="text" name="cedula" id="cedula" maxlength="10" class="form-control" placeholder="escribe una cedula" aria-describedby="helpId" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Fecha Nacimiento:</label>
                        <input type="date" name="birthday" id="birthday" class="form-control" aria-describedby="helpId" required>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Género:</label>
                        <select class="form-control selectpicker" name="id_gender" id="id_gender" required>
                            <option>Seleccione un genero</option>
                            @foreach ($generos as $gender)
                                <option value="{{$gender->id}}">{{$gender->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="float-right">
                <button class="btn btn-success" id="btnRegistrarEmpleado">Registrar</button>
                <a class="btn btn-warning" href="" id="btnBack">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@push('javascript')

    <!-- Petición ajax para guardar un registro en la base de datos -->
    <script>
        $('#btnRegistrarEmpleado').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let name        = $('#name').val();
            let last_name   = $('#last_name').val();
            let cedula      = $('#cedula').val();
            let birthday    = $('#birthday').val();
            let id_gender   = $('#id_gender').val();
            let _token      = $('input[name=_token]').val();
            let birthday2 = new Date(birthday);
            let date        = new Date();
            let years_old   =  Math.floor((date-birthday2) / (365.25 * 24 * 60 * 60 * 1000));
            //Petición ajax
            $.ajax({
                type: "POST",
                url: "{{route('employee.store')}}",
                data: {
                    name        : name,
                    last_name   : last_name,
                    cedula      : cedula,
                    birthday    : birthday,
                    id_gender   : id_gender,
                    years_old   : years_old,
                    _token      : _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Registro exitoso', 'Nuevo registro');
                        window.location.href = '/employee'
                    }, 1000);
                },
                error: function (err) {
                    if(err.status == 422){
                        let template = `
                            <div class="card alert-icon flex-1 items-center border-2 border-red-500 justify-center rounded-full mt-2">

                            `;

                        $.each(err.responseJSON.errors, function (i,error) {
                        template += `
                                <li class="ml-2 text-danger">
                                    ${error[0]}
                                </li>
                                `
                        });
                        template += `</div>`;
                        $('#tasks').html(template);
                    }
                }   
            });
        });
    </script>
    <script>
    $('#btnBack').on('click', function(e){
        $.ajax({
                type: "GET",
                url: "{{route('employee.index')}}",
                success:function(response){
                    window.location.href = '/employee'
                },
        });
    });
    </script>
@endpush


