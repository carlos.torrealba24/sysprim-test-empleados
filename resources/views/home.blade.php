@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                <div class="text-center" style="margin-top:250px">
                    <h1 class="text-primary">Bienvenido a la aplicación {{ Auth::user()->name }}</h1>
                    <p class="text-primary">Selecciona una opción</p>
                </div>
        </div>
    </div>
</div>
@endsection
