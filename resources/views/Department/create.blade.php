@extends('layouts.app')

@section('content')
    <div class="">
        <form action="" id="formDepartamento" method="post">
            <div>
                <ul id="tasks" class="mt-3 list-disc list-inside text-sm text-red-600">
     
                </ul>
            </div>
            @csrf  
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="escribe un nombre" aria-describedby="helpId" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Descripción:</label>
                        <input type="text" name="description" id="description" class="form-control" placeholder="escribe una descripción" aria-describedby="helpId" required>
                    </div>
                </div>
            </div>             
            <div class="float-right">
                <button class="btn btn-success" id="btnRegistrarDepartamento">Registrar</button>
                <a class="btn btn-warning" href="" id="btnBack">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@push('javascript')

    <!-- Petición ajax para guardar un registro en la base de datos -->
    <script>
        $('#btnRegistrarDepartamento').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let name        = $('#name').val();
            let description = $('#description').val();
            let _token      = $('input[name=_token]').val();
            //Petición ajax
            $.ajax({
                type: "POST",
                url: "{{route('department.store')}}",
                data: {
                    name        : name,
                    description : description,
                    _token      : _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Registro exitoso', 'Nuevo registro');
                        window.location.href = '/department'
                    }, 1000);
                },
                error: function (err) {
                    if(err.status == 422){
                        let template = `
                            <div class="card alert-icon flex-1 items-center border-2 border-red-500 justify-center rounded-full mt-2">

                            `;

                        $.each(err.responseJSON.errors, function (i,error) {
                        template += `
                                <li class="ml-2 text-danger">
                                    ${error[0]}
                                </li>
                                `
                        });
                        template += `</div>`;
                        $('#tasks').html(template);
                    }
                }   
            });
        });
    </script>
    <script>
    $('#btnBack').on('click', function(e){
        $.ajax({
                type: "GET",
                url: "{{route('employee.index')}}",
                success:function(response){
                    window.location.href = '/employee'
                },
        });
    });
    </script>
@endpush


